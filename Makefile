USE_IPV6 := 1

.PHONY: bootstrap
bootstrap: ssh-config
	./scripts/bootstrap.sh

.PHONY: update-home
update-home: known-hosts ssh-key.dec
	./scripts/update-home.sh

.PHONY: ssh
ssh: known-hosts ssh-config
	ssh -F ssh-config amazing-alpaca

known-hosts:
	if [[ "$(USE_IPV6)" == "1" ]]; \
	then ssh-keyscan -t ed25519 -H $(shell pass ipv6) > known-hosts; \
	else ssh-keyscan -t ed25519 -H $(shell pass ipv4) > known-hosts; \
	fi

ssh-key.dec:
	pass ssh-key > ssh-key.dec
	chmod 0600 ssh-key.dec

ssh-config: known-hosts ssh-key.dec
	echo "Host amazing-alpaca" > ssh-config
	echo "  User root" >> ssh-config
	if [[ "$(USE_IPV6)" == "1" ]]; \
	then echo "  HostName $(shell pass ipv6)" >> ssh-config;\
	else echo "  HostName $(shell pass ipv4)" >> ssh-config;\
	fi;
	echo "  IdentityFile $(CURDIR)/ssh-key.dec" >> ssh-config
	echo "  UserKnownHostsFile $(CURDIR)/known-hosts" >> ssh-config

clean:
	rm -f known-hosts ssh-key.dec ssh-config
