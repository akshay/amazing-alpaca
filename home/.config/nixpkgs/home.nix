{ pkgs, ... }:

let sources = import ./nix/sources.nix;

in {
  home.packages = [
    pkgs.htop
    pkgs.emacs # Using programs.emacs messes with spacemacs
  ];

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.neovim = {
    enable = true;
  };

  programs.git = {
    enable = true;
    aliases = {
      co = "checkout";
      st = "status";
      lol = "log --oneline --all --abbrev-commit --graph --decorate --color";
	    git = "!exec git";
    };
    extraConfig = {
      rebase.autoStash = true;
      pull.rebase = true;
    };
  };

  programs.zsh = {
    enable = true;
    profileExtra = "if [ -e /root/.nix-profile/etc/profile.d/nix.sh ]; then . /root/.nix-profile/etc/profile.d/nix.sh; fi";
    localVariables = { POWERLEVEL9K_CONFIG_FILE = "/root/.p10k.zsh"; };
    initExtra = "source /root/.p10k.zsh";
    initExtraBeforeCompInit = ''
      fpath+=(/usr/share/zsh/vendor-completions)
    '';
    prezto = {
      enable = true;
      prompt.theme = "powerlevel10k";
      pmodules = [
        "environment"
        "terminal"
        "editor"
        "history"
        "directory"
        "spectrum"
        "utility"
        "completion"
        "prompt"
        "syntax-highlighting"
        "history-substring-search"
      ];
      autosuggestions.color = "fg=blue";
      syntaxHighlighting.highlighters = [ "main" "brackets" "pattern" "line" "cursor" "root" ];
    };
  };

  programs.tmux = {
    enable = true;
    secureSocket = false;
    prefix = "C-b";
    escapeTime = 0;
    baseIndex = 1;
    keyMode = "vi";
    terminal = "screen-256color";
    plugins = with pkgs; [
      tmuxPlugins.cpu
      tmuxPlugins.continuum
      tmuxPlugins.prefix-highlight
      tmuxPlugins.fingers
      tmuxPlugins.sensible
      tmuxPlugins.pain-control
    ];
    extraConfig = builtins.readFile ./tmux.conf;
  };

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };

  programs.home-manager = {
    enable = true;
  };

  home.file = {
    ".emacs.d" = {
      source = sources.spacemacs;
      recursive = true;
    };
    ".spacemacs".text = builtins.readFile ./spacemacs;
    ".stack/config.yaml".text = builtins.readFile ./stack-config.yaml;
  };
}
