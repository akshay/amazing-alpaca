#!/usr/bin/env bash

set -euo pipefail

apt-get update
apt-get install -y curl dbus-user-session

# Remove preinstalled services
apt-get purge -y apache2 apache2-utils apache2-bin apache2-data apache2-doc
apt-get purge -y exim4 exim4-base exim4-config exim4-daemon-light

mkdir -p /etc/nix/
echo "build-users-group =" > /etc/nix/nix.conf
curl -L https://nixos.org/nix/install | sh
source "$HOME/.nix-profile/etc/profile.d/nix.sh"

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install

home-manager build
home-manager switch

readonly zshPath=/root/.nix-profile/bin/zsh
if ! cat /etc/shells | grep "$zshPath"; then
    echo "$zshPath" >> /etc/shells
fi
chsh -s "$zshPath" root
