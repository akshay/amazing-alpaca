let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in {
  env = pkgs.buildEnv {
    name = "alpaca";
    paths = [
      pkgs.pass
      pkgs.niv
      pkgs.gnumake
      pkgs.passh
      pkgs.rsync
    ];
  };
}
