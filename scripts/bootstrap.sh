#!/usr/bin/env bash

set -euo pipefail

readonly topDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
source "$topDir/scripts/util.sh"

"$topDir/scripts/sync-home.sh" "true"

ssh "${sshOpts[@]}" amazing-alpaca ./install-nix.sh
