#!/usr/bin/env bash

set -euo pipefail

readonly usePassword=${1:-"false"}

readonly topDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
source "$topDir/scripts/util.sh"
readonly rsyncOpts=(-avz -s -og --chown=root:root)

if [[ "$usePassword" == "true" ]]; then
    passh -p "$(pass password)" rsync "${rsyncOpts[@]}" -e "ssh ${sshOpts[*]}" "$topDir/home/." "amazing-alpaca:"
else
    rsync "${rsyncOpts[@]}" -e "ssh ${sshOpts[*]}" "$topDir/home/." "amazing-alpaca:"
fi
