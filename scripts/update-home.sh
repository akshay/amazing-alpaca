#!/usr/bin/env bash

set -euo pipefail

readonly topDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
source "$topDir/scripts/util.sh"

"$topDir/scripts/sync-home.sh" "false"
ssh "${sshOpts[@]}" amazing-alpaca 'home-manager build && home-manager switch'
